/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.sw.algoritmo2;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author dcarvajals
 */
public class Algoritmo2 {

    public static void main(String[] args) {
        // mayuscula
        Character mayu = abecedario();
        // minuscula
        Character minu = abecedario().toString().toLowerCase().charAt(0);
        // caracter especial
        Character especcial = caracterEspecial();

        String clave = claveGenerica();
        Set<Integer> pos = posiciones(clave);

        String claveFinal = "";
        // reemplazamos las posiciones aleatorias asegundo que cumpla con lo requerido la clave
        for(int i = 0; i < clave.length(); i++) {
            if((int) pos.toArray()[0] -1  == i) {
                claveFinal += mayu;
            } else if ((int) pos.toArray()[1] - 1  == i) {
                claveFinal += minu;
            } else if ((int) pos.toArray()[2] - 1 == i) {
                claveFinal += especcial;
            } else {
                claveFinal += clave.charAt(i);
            }
        }
        
        System.out.println("Clave generada: " + claveFinal);
        
    }

    public static Character abecedario() {
        String abecedario = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
        int position = new Random().nextInt(abecedario.length() + 1);
        return abecedario.charAt(position);
    }

    public static Character caracterEspecial() {
        String caracteresEspeciales = "!@#$%^&*()_-+={}[]:;<>,.?/~`|\\";
        int position = new Random().nextInt(caracteresEspeciales.length() + 1);
        return caracteresEspeciales.charAt(position);
    }

    public static String claveGenerica() {
        String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        int longitud = random.nextInt(8) + 8; // entre 8 al 15
        StringBuilder sb = new StringBuilder(longitud);

        for (int i = 0; i < longitud; i++) {
            int indice = random.nextInt(caracteres.length());
            sb.append(caracteres.charAt(indice));
        }

        return sb.toString();
    }

    public static Set posiciones(String clave) {
        Random random = new Random();
        Set<Integer> numerosAleatorios = new HashSet<Integer>();

        while (numerosAleatorios.size() < 3) {
            int aleatorio = random.nextInt(clave.length() - 1 + 1) + 1;
            numerosAleatorios.add(aleatorio);
        }
   
        return numerosAleatorios;
    }
}
