/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.sw.algoritmo1;

import java.util.Scanner;

/**
 *
 * @author dcarvajals
 */
public class Algoritmo1 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingresa una frase: ");
        String frase = scanner.nextLine();
        System.out.print("Ingresa un texto: ");
        String texto = scanner.nextLine();

        System.out.println("La frase ingresada es: " + frase);
        System.out.println("El texto ingresado es: " + texto);

        scanner.close();

        System.out.println(ordenarHaciaAtras(frase, texto));

    }

    public static String ordenarHaciaAtras(String frase, String texto) {
        String textoResponse = "";
        for (int x = frase.length() - 1; x >= 0; x--) {
            if (frase.charAt(x) == ' ') {
                textoResponse += texto;
            } else {
                textoResponse += frase.charAt(x);
            }
        }
        return textoResponse;
    }
}
