package com.viamatica.controllers;

import com.viamatica.entities.Product;
import com.viamatica.servicies.ProductServicie;
import com.viamatica.servicies.UserPersonServicie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductApi {

    @Autowired
    public ProductServicie productServicie;

    /**
     * getList() Funcion que me devuelve los productos registrados en la base de datos
     *
     * */
    @GetMapping("/getList")
    public ResponseEntity<List<Product>> getList() {
        List<Product> list = productServicie.getProducts();
        return ResponseEntity.ok(list);
    }

}
