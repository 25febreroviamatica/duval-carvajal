package com.viamatica.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@Table(name = "purchase")
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private Userperson idUser;

    @Size(max = 5)
    @Column(name = "number_order", length = 5)
    private String numberOrder;

    @Column(name = "datepurchase")
    private LocalDateTime datepurchase;

    @Column(name = "visible")
    private Boolean visible;

    @OneToMany(mappedBy = "idPurhcase")
    private Set<PurchaseDetail> purchaseDetails = new LinkedHashSet<>();

}