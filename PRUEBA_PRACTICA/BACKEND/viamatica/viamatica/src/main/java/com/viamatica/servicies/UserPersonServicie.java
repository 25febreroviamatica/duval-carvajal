package com.viamatica.servicies;

import com.google.gson.JsonObject;
import com.viamatica.entities.Userperson;
import com.viamatica.repository.UserpersonRepository;
import com.viamatica.util.Methods;
import com.viamatica.util.WeEncoder;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserPersonServicie {

    @Autowired
    private UserpersonRepository userpersonRepository;

    //@Autowired
    //private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private WeEncoder weEncoder;

    /**
     * Metodo para iniciar sesion dentro de la aplicacion logIn()
     * @param username nombre de usaurio
     * @param password contrasenia del usuaruio a ingresar
     *
     * */
    public String[] logIn(String username, String password) {
        System.out.println("logIn Controller");
        String status = "4", message = "Error in the parameters entered", data = "[]";
        System.out.println(username);
        List<Userperson> users = userpersonRepository.finByUserName(username);
        System.out.println(users);
        if(users.size() > 0) {
            // validar si las contrasenias son iguales
            if(users.get(0).getPassword().equals(password)) {
                status = "2";
                message = "User session successfully logged in.";
                data = "[" + personToJson(users.get(0)).toString() + "]";
            } else {
                status = "3";
                message = "The passwords do not match";
            }
        } else {
            status = "3";
            message = "The user is not registered in the database.";
        }

        System.out.println(message);
        return new String[]{status, message, data};
    }


    /**
     * Metodo para obtener en formato json la informacion del usuario que inicia sesion personToJson()
     * @param user objeto del usuario a iniciar sesion
     *
     * */
    public JsonObject personToJson(Userperson user) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_token", Methods.personToJson(user));
        jsonObject.addProperty("username", user.getUsername());
        jsonObject.addProperty("permit", user.getPermit());
        return jsonObject;
    }

}
