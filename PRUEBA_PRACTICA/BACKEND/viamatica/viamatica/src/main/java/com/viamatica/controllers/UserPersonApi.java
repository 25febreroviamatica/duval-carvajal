package com.viamatica.controllers;

import com.google.gson.JsonObject;
import com.viamatica.servicies.UserPersonServicie;
import com.viamatica.util.Methods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/userperson")
public class UserPersonApi {

    @Autowired
    public UserPersonServicie userPersonServicie;

    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<String> loginByEmail(@RequestBody @Validated String data) {
        System.out.println("logIn...");
        String message;
        JsonObject jso = Methods.stringToJSON(data);
        if (jso.size() > 0) {
            String email = Methods.JsonToString(jso, "username", "");
            String password = Methods.JsonToString(jso, "password", "");

            String[] res = userPersonServicie.logIn(email, password);

            message = Methods.getJsonMessage(res[0], res[1], res[2]);
            return new ResponseEntity<>(message, HttpStatus.OK);
        } else {
            message = Methods.getJsonMessage("4", "Parametros de entrada vacios.", "[]");
            return new ResponseEntity<>(message, HttpStatus.BAD_GATEWAY);
        }

    }

}
