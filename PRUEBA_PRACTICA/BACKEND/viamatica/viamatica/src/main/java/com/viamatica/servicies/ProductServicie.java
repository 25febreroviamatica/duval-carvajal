package com.viamatica.servicies;

import com.viamatica.entities.Product;
import com.viamatica.repository.ProductRepository;
import com.viamatica.repository.UserpersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ProductServicie {

    @Autowired
    private ProductRepository productRepository;

    /**
     * getProducts() Lista de productos
     *
     * */
    public List<Product> getProducts () {
        return productRepository.finByTrue();
    }

}
