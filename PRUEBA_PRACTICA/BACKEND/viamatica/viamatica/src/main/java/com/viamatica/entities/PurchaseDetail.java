package com.viamatica.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@Table(name = "purchase_detail")
public class PurchaseDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_purhcase")
    private Purchase idPurhcase;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_producto")
    private Product idProducto;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "_commit")
    @Type(type = "org.hibernate.type.TextType")
    private String commit;

    @Column(name = "visible")
    private Boolean visible;

}