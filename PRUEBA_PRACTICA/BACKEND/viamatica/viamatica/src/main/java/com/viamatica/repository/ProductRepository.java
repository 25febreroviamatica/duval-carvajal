package com.viamatica.repository;

import com.viamatica.entities.Product;
import com.viamatica.entities.Userperson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query(value = "select * from product as prd where prd.visible=true", nativeQuery = true)
    List<Product> finByTrue();

}