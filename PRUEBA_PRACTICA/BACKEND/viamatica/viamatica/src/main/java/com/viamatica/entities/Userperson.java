package com.viamatica.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@Table(name = "userperson")
public class Userperson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Size(max = 60)
    @Column(name = "username", length = 60)
    private String username;

    @Column(name = "_password")
    @Type(type = "org.hibernate.type.TextType")
    private String password;

    @Size(max = 1)
    @Column(name = "permit", length = 1)
    private String permit;

    @Column(name = "visible")
    private Boolean visible;

    @OneToMany(mappedBy = "idUser")
    private Set<Purchase> purchases = new LinkedHashSet<>();

}