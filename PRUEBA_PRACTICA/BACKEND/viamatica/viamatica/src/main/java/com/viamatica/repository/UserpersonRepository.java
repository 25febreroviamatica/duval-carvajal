package com.viamatica.repository;

import com.viamatica.entities.Userperson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserpersonRepository extends JpaRepository<Userperson, Integer> {

    @Query(value = "select * from userperson as usr where usr.username=?1", nativeQuery = true)
    List<Userperson> finByUserName(String param);

}