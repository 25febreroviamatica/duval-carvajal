import {Component, OnInit} from '@angular/core';
import {Product} from "../model/Product";
import {HttpClient} from "@angular/common/http";
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import {PurchaseDetail} from "../model/PurchaseDetail";

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit{

  purchaseDetails: PurchaseDetail[];

  constructor(private _http: HttpClient, private formBuilder: FormBuilder, public router: Router) { }

  ngOnInit() {
    this.purchaseDetails = JSON.parse(sessionStorage.getItem("products"));
    console.log(this.purchaseDetails);
  }

}
