import {UserPerson} from "./UserPerson";

export interface Purchase {
  id: number,
  idUser: UserPerson,
  numberOrder: string,
  datepurchase: string,
  visible: boolean
}
