import {PurchaseDetail} from "./PurchaseDetail";

export interface Product {
  id: number,
  title: string,
  price: string,
  description: string,
  category: string,
  image: string,
  ratingRate: number,
  ratingCount: number,
  visible: boolean,
  purhcaseDetail: PurchaseDetail
}
