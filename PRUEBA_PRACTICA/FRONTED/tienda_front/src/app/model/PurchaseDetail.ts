import {Purchase} from "./Purchase";
import {Product} from "./Product";

export interface PurchaseDetail {
  id: number,
  idPurchase: Purchase,
  idProducto: Product,
  quantity: number,
  _commit: string,
  visible: boolean
}
