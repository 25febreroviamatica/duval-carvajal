import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit{

  quantityCar: number;

  ngOnInit() {
    this.updateQuantity();
  }

  constructor(public router: Router) {
  }

  closeSession () {
    sessionStorage.clear();
    this.router.navigateByUrl('/login');
  }

  updateQuantity () {
    let data = JSON.parse(sessionStorage.getItem("products"));
    this.quantityCar = data === null ? 0 : data.length;
  }

}
