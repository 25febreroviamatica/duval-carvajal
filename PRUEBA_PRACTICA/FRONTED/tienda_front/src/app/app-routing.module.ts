import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';
import { ProductComponent } from './product/product.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';
import { CarComponent } from './car/car.component';

const routes: Routes = [
  {path: "menu", component: MenuComponent},
  {path: "login", component: LoginComponent},
  {path: "menu/product", component: ProductComponent},
  {path: "menu/viewproduct", component: ViewproductComponent},
  {path: "menu/car", component: CarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
