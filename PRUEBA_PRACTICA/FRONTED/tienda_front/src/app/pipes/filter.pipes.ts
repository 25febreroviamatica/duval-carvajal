import {Pipe, PipeTransform} from "@angular/core";
import {filter} from "rxjs";

@Pipe({
  name: 'filter'
})
export class FilterPipes implements  PipeTransform {

  transform(value: any, arg: any): any {

    const result = [];
    if(value != undefined) {
      for (const post of value) {
        if (post.title.toString().toLowerCase().indexOf(arg.toString().toLowerCase()) > -1) {
          result.push(post);
        }

      }

    }
    return result;
  }


}
