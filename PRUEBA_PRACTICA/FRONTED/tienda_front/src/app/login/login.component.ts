import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  regForm: FormGroup;
  globalUri: string;

  get form() {
    return this.regForm.controls;
  }

  constructor(private _http: HttpClient, private formBuilder: FormBuilder, public router: Router) {
  }

  ngOnInit() {
    this.regForm = this.formBuilder.group(
      {
        username: ["", Validators.required],
        password: ["", Validators.required],
      }
    );
  }

  login () {
    console.log("login()");
    if (this.regForm.invalid) {
      alert("Invalid form, please enter the required fields");
      return;
    }

    let data = {
      username: this.form["username"].value,
      password: this.form["password"].value
    }

    console.log(data);

    this.loginApi(data).subscribe(response => {
      console.log(response)
      if(response.status === 2) {
        sessionStorage.setItem("username", response.data[0].username);
        sessionStorage.setItem("permit", response.data[0].permit);
        this.router.navigateByUrl('/menu/product');
      } else {
        alert(response.information);
      }
    })
  }

  loginApi(user: {}): Observable<any> {
    this.globalUri = "http://localhost:8080/viamatica_bk/userperson/login";
    console.log(this.globalUri);
    return this._http.post(this.globalUri, user, {});
  }

}
