import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Product} from "../model/Product";
import {Observable} from "rxjs";
import {PurchaseDetail} from "../model/PurchaseDetail";
import {MenuComponent} from "../menu/menu.component";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit{

  @ViewChild(MenuComponent) menu:any;

  regForm: FormGroup;
  globalUri: string;
  filterProduct:string = "";

  product: Product;
  products: Product[];

  purchaseDetails: PurchaseDetail[];

  sesion: string;

  get form() {
    return this.regForm.controls;
  }

  constructor(private _http: HttpClient, private formBuilder: FormBuilder, public router: Router) {
  }

  ngOnInit() {
    let data = JSON.parse(sessionStorage.getItem("products"));
    this.purchaseDetails = data === undefined ? [] : data;
    this.sesion = sessionStorage.getItem('username');
    this.getList();
    this.regForm = this.formBuilder.group(
      {
        id: ["", Validators.required],
        title: ["", Validators.required],
        price: [0, Validators.required],
        description: ["", Validators.required],
        category: ["", Validators.required],
        image: ["", Validators.required],
        ratingRate: [0, Validators.required],
        ratingCount: [0, Validators.required],
        visible: [true, Validators.required],
      }
    );
  }

  addCar(item: Product) {

    if(this.purchaseDetails === null)
      this.purchaseDetails = [];

    this.purchaseDetails.push({
      id: 0,
      idProducto: item,
      idPurchase: undefined,
      quantity: 1,
      _commit: "",
      visible: true
    });
    sessionStorage.setItem("products", JSON.stringify(this.purchaseDetails));
    console.log(sessionStorage.getItem("products"));
    this.menu.updateQuantity();
    alert("Producto add: " + item.title +" - Price: $" + item.price);
  }

  viewProduct (item: Product) {
    sessionStorage.setItem("productView", JSON.stringify(item));
    console.log(sessionStorage.getItem("productView"));
    this.router.navigateByUrl('/menu/viewproduct');
  }

  getList() {
    this.getListsApi().subscribe(response => {
      console.log(response);
      this.products = response;
    });
  }

  getListsApi(): Observable<any> {
    this.globalUri = "http://localhost:8080/viamatica_bk/product/getList";
    return this._http.get<any>(this.globalUri, {});
  }

}
