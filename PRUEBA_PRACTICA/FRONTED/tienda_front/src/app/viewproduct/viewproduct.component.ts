import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import {Product} from "../model/Product";

@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.css']
})
export class ViewproductComponent implements OnInit{

  producto: Product;

  constructor(private _http: HttpClient, private formBuilder: FormBuilder, public router: Router) { }

  ngOnInit() {
    this.producto = JSON.parse(sessionStorage.getItem("productView"));
    console.log(this.producto);
  }


}
